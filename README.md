# Diagramme
```mermaid
classDiagram
	           
  
   
  
   Entreprise-->"*"employé:Listemployés
  
  Manager ..|>IDataManager 
  Manager -->"*" Entreprise 
   Manager -->"*"employé :+ListMaquette
   
    IDataManager <|.. IentrepriseDbManager
   IDataManager <|.. IemployeDbManager
 
   
    IentrepriseDbManager <|.. Stubentreprise
   IemployeDbManager <|.. Stubemploye
   
  
   IentrepriseDbManager <|.. entrepriseDbDataManager
   IemployeDbManager <|.. employeDbDataManager
   employeDbDataManager..>employeEntity
   entrepriseDbDataManager..>entrepriseEntity
  
   
   entrepriseDbDataManager..>premierpartieContext 
   employeDbDataManager..>premierpartieContext 
  
   
   
 
 class Entreprise{
 +string nom
 +int siret
 +Ajouteremploye() (List~employé~ employés)  
}
class employé{
     +string nom
    +string prenom
    +int salaire
    +Datetime  datenaissance
    + string numerosecurite  
}



class Manager{
         
      }
      class IDataManager{
        <<interface>>
        + Add(Data data) 
        + Delete(Data data)
        + Update(Data data)
        + GetDataWithName(string name)
        + GetAll()        
    }

    class IentrepriseDbManager{
        <<interface>>
        
        + Addemployeentreprise(EntrepriseModel ent,   Employe employe )        
    }
   
    class IemployeDbManager{
        <<interface>>       
        
    }
    class Manager{
         +Addentreprise(entreprise  entreprise)
        
      }
      class IDataManager{
        <<interface>>
        + Add(Data data) 
        + Delete(Data data)
        + Update(Data data)
        + GetDataWithName(string name)
        + GetAll()        
    }
   
      
      
 
class Stubemploye{
    
   
}
class Stubentreprise{
    
   
}

```

## DESCR

   * Une entreprise représenté par la table **entreprise** est constituée de plusieurs employés.
   * Un employe  représenté par la table **employe** .
   
   
  * Le manageur (**MANAGER**) nous sert de gestionnaire , il implémente les méthodes de traitement pour l'implémentation des fonctionnalités de notre application, il s'agit des traitements qui nécessite l'interrogation de la base de données via la base de données(**IDataManager**). Il sert d'intermédiaire pour le programme d'accéder à la partie conception de l'application.
  
  * La gestion du stockage des données ainsi que leur manipulation ont été faites avec l'ORM **Entity Framework**
  * Les classes **Stubentreprise**, **Stubemploye**servent de fournisseur de données pour les tests unitaires.
  
  * La classe **premierpartieContext** est composée de la chaîne de connection à la base de données et permet de lier les tables aux entités.
  * Les classes **UeemployeDataManager**, **UeentrepriseDbDataManager**utilisent respectivement les classes  **entrepriseEntity**, **employeEntity** pour manipuler les données principalement sur l'insertion, la modification, la suppression et la sélection.
 
 





